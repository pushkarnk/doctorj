package org.incava.doctorj;

import java.io.*;
import java.util.*;
import junit.framework.TestCase;
import org.incava.analysis.*;
import org.incava.java.*;
import org.incava.log.Log;


public class Tester extends TestCase
{
    public Tester(String name)
    {
        super(name);
        Options.warningLevel = Options.MAXIMUM_WARNING_LEVEL;
    }

    public void evaluate(String contents, Object[][] expectations)
    {
        StringWriter       reportOutput = new StringWriter();
        Report             report   = new TerseReport(reportOutput);
        JavaParserVisitor  analyzer = new JavadocAnalyzer(report);
        try {
            report.reset(contents);

            Java13Parser       parser   = new Java13Parser(new StringReader(contents));
            ASTCompilationUnit cu       = parser.CompilationUnit();

            // cu.dump();

            cu.jjtAccept(analyzer, null);
            Set violations = report.getViolations();
            {
                Iterator vit = violations.iterator();
                while (vit.hasNext()) {
                    Log.log("violation: " + vit.next());
                }
            }
            
            assertEquals("number of violations", expectations.length, violations.size());
            Iterator vit = violations.iterator();
            for (int vi = 0; vit.hasNext() && vi < violations.size(); ++vi) {
                Violation violation = (Violation)vit.next();
                assertNotNull("violation not null", violation);
                assertEquals("expectations length #" + vi, 5, expectations[vi].length);
                
                String msg  = (String)expectations[vi][0];
                assertEquals("violation message #" + vi, msg, violation.message);
                int    line = ((Integer)expectations[vi][1]).intValue();
                assertEquals("begin line #" + vi, line, violation.beginLine);
                if (expectations[vi].length > 2) {
                    int col = ((Integer)expectations[vi][2]).intValue();
                    assertEquals("begin column #" + vi, col, violation.beginColumn);
                    if (expectations[vi].length > 3) {
                        col = ((Integer)expectations[vi][3]).intValue();
                        assertEquals("end line #" + vi, col, violation.endLine);
                        if (expectations[vi].length > 4) {
                            col = ((Integer)expectations[vi][4]).intValue();
                            assertEquals("end column #" + vi, col, violation.endColumn);
                        }
                    }
                }
            }

            report.flush();
        }
        catch (ParseException e) {
            System.out.println(e.getMessage());
            System.out.println("Encountered errors during parse.");
            fail(e.getMessage());
        }
    }
    
    public void evaluate(String contents, List expectations)
    {
        StringWriter       reportOutput = new StringWriter();
        Report             report   = new TerseReport(reportOutput);
        JavaParserVisitor  analyzer = new JavadocAnalyzer(report);
        try {
            report.reset(contents);
            
            Java13Parser       parser   = new Java13Parser(new StringReader(contents));
            ASTCompilationUnit cu       = parser.CompilationUnit();

            // cu.dump();

            cu.jjtAccept(analyzer, null);
            Set violations = report.getViolations();
            assertEquals("number of violations", expectations.size(), violations.size());
            
            Iterator eit = expectations.iterator();
            Iterator vit = violations.iterator();
            for (int vi = 0; vit.hasNext() && vi < violations.size(); ++vi) {
                Violation   violation = (Violation)vit.next();
                Expectation exp       = (Expectation)eit.next();
                
                assertNotNull("violation not null", violation);
                
                assertEquals("violation message", exp.message,     violation.message);
                assertEquals("begin line",        exp.beginLine,   violation.beginLine);
                assertEquals("begin column",      exp.beginColumn, violation.beginColumn);
                assertEquals("end line",          exp.endLine,     violation.endLine);
                assertEquals("end column",        exp.endColumn,   violation.endColumn);
            }

            report.flush();
        }
        catch (ParseException e) {
            System.out.println(e.getMessage());
            System.out.println("Encountered errors during parse.");
            fail(e.getMessage());
        }
    }
    
}
