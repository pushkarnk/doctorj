package org.incava.doctorj;

import junit.framework.TestCase;


public class TestCtorDocAnalyzer extends Tester
{
    public TestCtorDocAnalyzer(String name)
    {
        super(name);
    }

    public void testReturnTag()
    {
        evaluate("/** This is a description. */\n" +
                 "class Test {\n" +
                 "    /** This is a description.\n" +
                 "      * @return Something\n" +
                 "      */\n" +
                 "    Test() {}\n" +
                 "}\n",
                 new Object[][] { 
                     { "Tag not valid for constructor", new Integer(4), new Integer(9), new Integer(4), new Integer(15) }
                 });
    }

}
